class CuddlyConfig
	attr_accessor :port, :webroot
	def initialize
		@port = 8080
		@webroot = './serve'
	end
end
