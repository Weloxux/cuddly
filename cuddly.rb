require 'socket'
require_relative 'config'
require_relative 'helper'

$headers = {'success' => 'HTTP/1.1 200 OK\r\nServer: cuddly\r\nContent-Type: text/html; charset=iso-8859-1\r\nConnection: close\r\n',
	'notfound' => 'HTTP/1.1 404 Not Found\r\nServer: cuddly\r\nContent-Type: text/html; charset=iso-8859-1\r\nConnection: close\r\n'}

$c = CuddlyConfig.new

$server = TCPServer.new($c.port)

loop do
	socket = $server.accept
	message = ''

	# Prepare answer to client
	filepath = $c.webroot + socket.gets.split(' ')[1]
	if File.file?(filepath)
		if filepath.include? '..'
			info('Forbidden')
			header = $headers['forbidden']
			message = 'You cannot access that. Sorry!'
		else
			info('Success')
			header = $headers['success']
			File.open(filepath, 'r') do |file|
				file.each_line do |line|
					message += line
				end
			end
		end
	elsif !File.file?(filepath)
		info('Not found')
		header = $headers['notfound']
		message = 'File not found'
	end

	# Send answer
	socket.puts header + '\r\n'
	socket.puts message

	# Close connection
	socket.close
end
